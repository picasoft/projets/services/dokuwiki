FROM nginx:stable
LABEL maintainer="quentinduchemin@tuta.io"

WORKDIR /var/www

# Add PHP 8 repository
RUN apt-get update -y && \
    apt-get install -y apt-transport-https ca-certificates gnupg2 lsb-release curl && \
    curl -fsSL https://packages.sury.org/php/apt.gpg | apt-key add - && \
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list && \
    apt-get clean

RUN apt-get update -y && \
    apt-get install -y \
    # See https://team.picasoft.net/picasoft/pl/awehsj534jnypc6czotjywfg8c
    libldap-common \
    tar \
    vim \
    locales \
    locales-all \
    php8.2 \
    php8.2-fpm \
    php8.2-gd \
    php8.2-mbstring \
    php8.2-ldap \
    php8.2-xml && \
    mkdir -p html /var/run/php && \
    echo "cgi.fix_pathinfo = 0;" >> /etc/php/8.2/fpm/php.ini && \
    sed -i -e "s|;daemonize\s*=\s*yes|daemonize = no|g" /etc/php/8.2/fpm/php-fpm.conf && \
    sed -i -e "s|listen\s*=\s*127\.0\.0\.1:9000|listen = /var/run/php-fpm7.sock|g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i -e "s|;listen\.owner\s*=\s*|listen.owner = |g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i -e "s|;listen\.group\s*=\s*|listen.group = |g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i -e "s|;listen\.mode\s*=\s*|listen.mode = |g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i -e "s|;extension=mbstring|extension=mbstring|g" /etc/php/8.2/fpm/php.ini && \
    sed -i -e "s|;date\.timezone =|date\.timezone = Europe/Paris|g" /etc/php/8.2/fpm/php.ini && \
    curl -fsSL -o dokuwiki.tgz "https://download.dokuwiki.org/src/dokuwiki/dokuwiki-2024-02-06b.tgz" && \
    mkdir update && \
    tar -xzf dokuwiki.tgz -C update --strip-components 1 && \
    rm -f dokuwiki.tgz && \
    apt-get autoremove -y && \
    apt-get clean

# Latest releases available at https://github.com/aptible/supercronic/releases
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.2.30/supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=9f27ad28c5c57cd133325b2a66bba69ba2235799

RUN curl -fsSLo /usr/local/bin/supercronic "$SUPERCRONIC_URL" \
    && echo "${SUPERCRONIC_SHA1SUM} /usr/local/bin/supercronic" | sha1sum -c - \
    && chmod +x /usr/local/bin/supercronic

ENV LC_ALL fr_FR.UTF-8
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR.UTF-8

COPY mime.local.conf /
COPY --chown=www-data run.sh /

RUN chown -R www-data . && \
    chmod +x /run.sh 

EXPOSE 80
VOLUME ["/var/www/html"]

CMD ["/run.sh"]
