#!/usr/bin/env bash

# EXPLAINING THE SCRIPT run.sh
# This is a poor man's supervisord. The only thing this script does is watching its forked (background) processes and as soon as one dies, it terminates all the others and exits with the code of the first dying process.
# see : https://github.com/dinkel/docker-nginx-phpfpm

set -m

echo "Copy static Dokuwiki files on top of existing pages..."
cp -afP /var/www/{update/*,html/}

echo "Remove old files..."
grep -Ev '^($|#)' /var/www/html/data/deleted.files | xargs -n 1 rm -vf

echo "Remove wiki tutorial pages..."
rm -rf /var/www/html/data/pages/{wiki,playground,pagecreator}

# See https://team.picasoft.net/picasoft/pl/d1upfor98fynzkdkfq3j5sssjw
echo "Remove cache to avoid update incompatibilites..."
rm -rf /var/www/html/data/cache/*

echo "Add additional MIME types..."
cp /mime.local.conf /var/www/html/conf/

echo "Setting up crons"
echo "0 0 * * * /usr/lib/php/sessionclean" > /crontab.conf

echo "Launching Dokuwiki..."
php-fpm8.2 &
nginx &

echo "Start supercronic..."
supercronic /crontab.conf &

pids=`jobs -p`

exitcode=0

function terminate() {
	trap "" CHLD

	for pid in $pids; do
		if ! kill -0 $pid 2>/dev/null; then
			wait $pid
			exitcode=$?
		fi
	done

	kill $pids 2>/dev/null
}

trap terminate CHLD
wait

exit $exitcode
