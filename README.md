## Dokuwiki

Ce dossier contient les ressources nécessaires pour lancer une instance de Dokuwiki.
Nous maintenons une image de ce service.

Pour Dokuwiki, tout est fichier, il n'y a pas de bases de données : l'administration est donc très simple.

### Configuration

La configuration se fait directement dans Dokuwiki, une fois lancé. Il n'y a pas de manière simple de pré-configurer Dokuwiki via un fichier de configuration. Il y a trop de paramètres, et mettre à jour ce dépôt à chaque changement de paramètre serait très peu pratique.

### Ajouter une extension autorisée

Par défaut, seuls certains types de fichiers [sont autorisés](https://www.dokuwiki.org/mime). Pour ajouter un type autorisé, éditer le fichier [mime.local.conf](./mime.local.conf), ajouter l'extension du fichier et son type MIME autorisé (voir la liste [ici](https://filext.com/)), puis reconstruire l'image.

### Lancement

Un simple `docker-compose up -d` suffit.

### Mise à jour

#### Dokuwiki

Il faudra changer la release téléchargée et extraite dans le [Dockerfile](./Dockerfile), en utilisant les liens présents sur la [page des archives](https://download.dokuwiki.org/archive).

N'utilisez jamais l'URL "stable" qui ne permet pas de build reproductibles.


Notez que tous les fichiers Dokuwiki sont "mélangés" : le code de Dokuwiki ainsi que les pages utilisateur. Tout se trouve dans `/var/www/html`. Ainsi, ce que cette image Docker fait pour permettre les mises à jour est :

* Récupérer la dernière version stable de Dokuwiki dans un dossier spécial
* Au lancement, copier le code de Dokuwiki dans `/var/www/html`
* Supprimer les fichiers supprimés par la nouvelle release

Ainsi, les anciens fichiers de code sont remplacés et les pages utilisateur ne bouge pas.

Cette méthode est inspirée de la [page de documentation de Dokuwiki sur les mises à jour](https://www.dokuwiki.org/install:upgrade).

Il faudra veiller à faire une passe sur les [paramètres de configuration](https://wiki.picasoft.net/doku.php?id=start&do=admin&page=config) pour vérifier qu'il n'y en a pas de nouveaux à définir (marqués en `!!not-set!!` par exemple).

#### Plugins

Les plugins doivent être mis à jour "manuellement", ils ne sont pas inclus dans le Dockerfile.

Le [gestionnaire d'extensions](https://wiki.picasoft.net/doku.php?id=start&do=admin&page=extension) permet cette mise à jour avec une interface graphique.

Souvent, les plugins sont mis à jours pour s'adapter au code des nouvelles versions de Dokuwiki. Après une mise à jour, il faut donc impérativement visiter cette page et mettre à jour les plugins détectés comme trop anciens.

Certains plugins peuvent également être incompatibles avec une mise à jour. On le verra dans les logs. Il faut alors chercher un patch ou désactiver le plugin.

Attention, il faut aussi vérifier les [templates](https://wiki.picasoft.net/doku.php?id=start&do=admin&page=extension&tab=templates).
